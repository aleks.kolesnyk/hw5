import happyFamily.Family;
import happyFamily.Human;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class FamilyTests {

    private Family module;
    private Human mother = new Human("Mother", "Mother", 1988);
    private Human father = new Human("Father", "Father", 1986);
    private Human child1 = new Human("child1", "child1", 2002);
    private Human child2 = new Human("child2", "child2", 2004);
    private Human child3 = new Human("child3", "child3", 1998);

    private Human[] children = {child1, child2, child3};

    @BeforeEach
    public void setUp() {

        module = new Family(mother, father);
    }

    @Test
    public void testDeleteChild() {

        module.setChildren(this.children);
        module.deleteChild(child1);
        int actual = module.getChildren().length;
        int expected = 2;
        Assertions.assertEquals(actual, expected);
    }


    @Test
    public void testDontDeleteChild() {

        module.setChildren(this.children);
        module.deleteChild(new Human());
        int actual = module.getChildren().length;
        int expected = 3;
        Assertions.assertEquals(actual, expected);
    }


    @Test
    public void testAddChild() {

        module.setChildren(this.children);
        module.addChild(new Human());
        int actual = module.getChildren().length;
        int expected = 4;
        Assertions.assertEquals(actual, expected);
    }

    @Test
    public void testCountFamily() {

        module.setChildren(this.children);
        int actual = module.countFamily();
        ;
        int expected = 5;
        Assertions.assertEquals(actual, expected);
    }

    @Test
    public void testToString() {

        String actual = module.toString();
        String expected = "{Mother=Human{name=Mother, surname=Mother, year=1988, iq=0, schedule=no_schedule} " +
                "Father=Human{name=Father, surname=Father, year=1986, iq=0, schedule=no_schedule} Children =[]}";
        Assertions.assertEquals(actual, expected);
    }
}


