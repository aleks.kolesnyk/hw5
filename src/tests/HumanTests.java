import happyFamily.Human;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class HumanTests {

    private Human module;

    @BeforeEach
    public void setUp() {

        module = new Human();
    }

    @Test
    public void testToString() {

        String actual = module.toString();
        String expected = "Human{name=, surname=, year=0, iq=0, schedule=no_schedule}";
        assertEquals(actual, expected);
    }

    @Test
    public void testEquals() {

        Human human1 = new Human("Test", "Equals", 2022);
        Human human2 = new Human("Test", "Equals", 2022);
        assertEquals(human1, human2);

    }


}
