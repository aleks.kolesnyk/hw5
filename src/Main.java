import happyFamily.DayOfWeek;
import happyFamily.Human;


public class Main {

    public static void main(String[] args) {
        String[][] schedule = {
                {DayOfWeek.SUNDAY.name(), "one more day for relax"},
                {DayOfWeek.MONDAY.name(), "go to work"},
                {DayOfWeek.THURSDAY.name(), "read a book"},
                {DayOfWeek.WEDNESDAY.name(), "watch a movie"},
                {DayOfWeek.THURSDAY.name(), "upgrade your CV"},
                {DayOfWeek.FRIDAY.name(), "go to gym"},
                {DayOfWeek.SATURDAY.name(), "relax"}
        };


        for (int i = 0; i < 200000; i++) {
            Human human = new Human("Try", "Finalize", i);
        }


    }

}
