package happyFamily;

import java.util.Arrays;

public class Human {

    private Family family;
    private String name = "";
    private String surname = "";
    private int year;
    private int iq;
    private String[][] schedule = new String[0][0];


    public Human() {}

    public Human(String name, String surname, int year) {

        this.name = name;
        this.surname = surname;
        this.year = year;
    }


    public Human(String name, String surname, int year, int iq, String[][] schedule) {

        this.name = name;
        this.surname = surname;
        this.year = year;
        this.iq = iq;
        this.schedule = schedule;
    }

    public void greetPet() {

        System.out.println("Привет, " + family.getPet().getNickname());
    }

    public void describePet() {

        if (family.getPet().getTrickLevel() > 50) {
            System.out.println("У меня есть " + family.getPet().getSpecies() + ", ему " + family.getPet().getAge() + " лет, " + "он очень хитрый");
        } else if (family.getPet().getTrickLevel() <= 50) {
            System.out.println("У меня есть " + family.getPet().getSpecies() + ", ему " + family.getPet().getAge() + " лет, " + "почти не хитрый");
        }
    }

    @Override
    public String toString() {

        return String.format("Human{name=%s, surname=%s, year=%d, iq=%d, schedule=%s}", name, surname, year, iq, schedule.length > 0 ? Arrays.deepToString(schedule) : "no_schedule");

    }

    @Override
    public boolean equals(Object obj) {

        if (this == obj) return true;

        if (obj == null || getClass() != obj.getClass()) return false;

        Human human = (Human) obj;

        return name.equals(human.getName()) && surname.equals(human.getSurname()) && year == human.year && family == human.getFamily();
    }

    @Override
    protected void finalize() throws Throwable {

        System.out.println(this.toString());
        super.finalize();
    }


    public String getName() {

        return name;
    }

    public void setName(String name) {

        this.name = name;
    }

    public String getSurname() {

        return surname;
    }

    public void setSurname(String surname) {

        this.surname = surname;
    }

    public int getYear() {

        return year;
    }

    public void setYear(int year) {

        this.year = year;
    }

    public int getIq() {

        return iq;
    }

    public void setIq(int iq) {

        this.iq = iq;
    }

    public String[][] getSchedule() {

        return schedule;
    }

    public void setSchedule(String[][] schedule) {

        this.schedule = schedule;
    }

    public Family getFamily() {

        return family;
    }

    public void setFamily(Family family) {

        this.family = family;
    }
}
