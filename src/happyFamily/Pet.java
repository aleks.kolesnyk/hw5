package happyFamily;


public class Pet {

    private Species species;
    private String nickname;
    private int age;
    private int trickLevel;
    private String[] habits;

    public Pet() {}

    public Pet(Species species, String nickname) {

        this.species = species;
        this.nickname = nickname;
    }

    public Pet(Species species, String nickname, int age, int trickLevel, String[] habits) {

        this.species = species;
        this.nickname = nickname;
        this.age = age;
        this.trickLevel = trickLevel;
        this.habits = habits;
    }

    public void eat() {

        System.out.println("Я кушаю!");
    }

    public void respond() {

        System.out.println("Привет, хозяин. Я - " + this.nickname + ". Я соскучился!");
    }

    public void foul() {

        System.out.println("Нужно хорошо замести следы...");
    }

    @Override
    public String toString() {

        return this.species + "{nickname='" + this.nickname + "', age=" + this.age +
                ", trickLevel=" + this.trickLevel + ", habits=" + this.habits + "}";
    }

    @Override
    public boolean equals(Object obj) {

        if (this == obj) return true;

        if (obj == null || getClass() != obj.getClass())
            return false;

        Pet pet = (Pet) obj;

        return species.equals(pet.getSpecies())
                && nickname.equals(pet.getNickname())
                && age == pet.age;
    }

    @Override
    protected void finalize() throws Throwable {

        System.out.println(this.toString());
        super.finalize();
    }

    public void setSpecies(Species species) {

        this.species = species;
    }

    public Species getSpecies() {

        return species;
    }

    public String getNickname() {

        return nickname;
    }

    public void setNickname(String nickname) {

        this.nickname = nickname;
    }

    public int getAge() {

        return age;
    }

    public void setAge(int age) {

        this.age = age;
    }

    public int getTrickLevel() {

        return trickLevel;
    }

    public void setTrickLevel(int trickLevel) {

        this.trickLevel = trickLevel;
    }

    public String[] getHabits() {

        return habits;
    }

    public void setHabits(String[] habits) {

        this.habits = habits;
    }
}
