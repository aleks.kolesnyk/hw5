package happyFamily;

import java.util.Arrays;

public class Family {

    Human mother = new Human();
    Human father = new Human();
    Human[] children = new Human[0];
    Pet pet = new Pet();

    public Family(Human mother, Human father) {

        this.mother = mother;
        this.father = father;
        mother.setFamily(this);
        father.setFamily(this);

    }

    @Override
    public String toString() {

        return "{Mother=" + this.mother + " Father=" + this.father + " Children =" + Arrays.toString(children) + "}";
    }

    @Override
    public boolean equals(Object obj) {

        if (this == obj) return true;

        if (obj == null || getClass() != obj.getClass())
            return false;

        Family family = (Family) obj;

        return family.getMother().getFamily() == this
                && family.getFather().getFamily() == this;
    }


    public void addChild(Human child) {

        int length = this.children.length;
        Human[] children = new Human[length + 1];

        for (int i = 0; i < length; i++) {
            children[i] = this.children[i];
        }

        child.setFamily(this);
        children[length] = child;

        this.children = children;
    }

    public boolean deleteChild(int index) {

        if (index < 0 || index >= this.children.length) {
            return false;
        }

        int length = this.children.length;
        Human[] children = new Human[length - 1];

        for (int i = 0; i < length; i++) {
            if (i == index) {
                continue;
            }
            children[i] = this.children[i];
        }

        this.children = children;
        return true;
    }
    public boolean deleteChild(Human child) {
        if (this.children == null) {
            return false;
        }
        Human[] previousChildren = this.getChildren().clone();
        Human[] newChildren;
        for (Human kid : previousChildren) {
            if (kid.equals(child)) {
                newChildren = new Human[this.children.length - 1];
                int j = 0;
                for (Human previousChild : previousChildren) {
                    if (previousChild.equals(child)) {
                        continue;
                    }
                    newChildren[j] = previousChild;
                    j++;
                }
                this.setChildren(newChildren);
                return true;
            }
        }
        return false;
    }


    public int countFamily() {

        return this.children.length + 2;
    }

    @Override
    protected void finalize() throws Throwable {

        System.out.println(this.toString());
        super.finalize();
    }


    public Human getMother() {

        return mother;
    }

    public void setMother(Human mother) {

        this.mother = mother;
    }

    public Human getFather() {

        return father;
    }

    public void setFather(Human father) {

        this.father = father;
    }

    public Human[] getChildren() {

        return children;
    }

    public void setChildren(Human[] children) {

        this.children = children;
    }

    public Pet getPet() {

        return pet;
    }

    public void setPet(Pet pet) {

        this.pet = pet;
    }
}
